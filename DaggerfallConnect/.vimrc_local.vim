autocmd BufEnter * setlocal tabstop=4     " number of spaces for <Tab>
autocmd BufEnter * setlocal softtabstop=4 " tabs at start of line
autocmd BufEnter * setlocal shiftwidth=4  " number of cols for indent operations
" no automatic newlines
autocmd BufEnter * setlocal formatoptions-=t
autocmd BufEnter * setlocal formatoptions-=c
autocmd BufEnter * setlocal formatoptions-=l
