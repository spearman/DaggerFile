﻿// Project:         Daggerfall Tools For Unity
// Copyright:       Copyright (C) 2009-2018 Daggerfall Workshop
// Web Site:        http://www.dfworkshop.net
// License:         MIT License (http://www.opensource.org/licenses/mit-license.php)
// Source Code:     https://github.com/Interkarma/daggerfall-unity
// Original Author: Gavin Clayton (interkarma@dfworkshop.net)
// Contributors:    
// 
// Notes:
//

#region Using Statements
using System.IO;
using DaggerfallConnect.Arena2;
#endregion

namespace DaggerfallConnect.Utility
{
    /// <summary>
    /// Static methods to validate ARENA2 folder.
    /// Does not verify contents, just that critical files exist in minimum quantities.
    /// This allows test to be fast enough to be run at startup.
    /// </summary>
    public class DFValidator
    {
        #region Fields

        const string textureSearchPattern = "TEXTURE.???";
        const string vidSearchPattern = "*.VID";
        const string vidAlternateTestFile = "ANIM0011.VID";

        const int minTextureCount = 472;
        const int minVidCount = 17;

        #endregion

        #region Structures

        /// <summary>
        /// Packages validation information.
        /// </summary>
        public struct ValidationResults
        {
            /// <summary>The full path that was tested.</summary>
            public string PathTested;

            /// <summary>True if all tests succeeded.</summary>
            public bool AppearsValid;

            /// <summary>True if folder exists.</summary>
            public bool FolderValid;

            /// <summary>True if texture count is correct.</summary>
            public bool TexturesValid;

            /// <summary>True if DAGGER.SND exists.</summary>
            public bool SoundsValid;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Validates an ARENA2 folder.
        ///  This currently just checks the right major files exist in the right quantities.
        ///  Does not verify contents so test is quite speedy and can be performed at startup.
        ///  Will also look for main .BSA files in Unity Resources folder.
        /// </summary>
        /// <param name="path">Full path of ARENA2 folder to validate.</param>
        /// <param name="results">Output results.</param>
        /// <param name="requireVideos">Videos must be present to pass final validation.</param>
        public static void ValidateArena2Folder(string path, out ValidationResults results, bool requireVideos = false)
        {
            results = new ValidationResults();
            results.PathTested = path;

            // Check folder exists
            if (string.IsNullOrEmpty(path) || !Directory.Exists(path))
                return;
            else
                results.FolderValid = true;

            // Get files
            string[] textures = Directory.GetFiles(path, textureSearchPattern);
            string[] sounds = Directory.GetFiles(path, SndFile.Filename);

            // Validate texture count
            if (textures.Length >= minTextureCount)
                results.TexturesValid = true;

            // Validate sounds count
            if (sounds.Length >= 1)
                results.SoundsValid = true;

            // If everything else is valid then set AppearsValid flag
            if (results.FolderValid &&
                results.TexturesValid &&
                results.SoundsValid)
            {
                results.AppearsValid = true;
            }
        }

        #endregion

    }
}
