#region Imports
using System;
#endregion

namespace DaggerfallConnect.Utility
{
    [Serializable]
    public struct Color {
        public float r;
        public float g;
        public float b;
        public float a;
        public Color(float r, float g, float b, float a) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }
        public static implicit operator Color32(Color color) {
            byte r = Convert.ToByte(Math.Round(color.r * 255.0));
            byte g = Convert.ToByte(Math.Round(color.g * 255.0));
            byte b = Convert.ToByte(Math.Round(color.b * 255.0));
            byte a = Convert.ToByte(Math.Round(color.a * 255.0));
            return new Color32(r, g, b, a);
        }
        public static Color clear = new Color(0,0,0,0);
        public static Color white = new Color(1,1,1,1);
    }
    [Serializable]
    public struct Color32 {
        public byte r;
        public byte g;
        public byte b;
        public byte a;
        public Color32(byte r, byte g, byte b, byte a) {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }
        public static implicit operator Color(Color32 color32) {
            return new Color(color32.r, color32.g, color32.b, color32.a);
        }
    }
}
