# DaggerfallConnect

partial DaggerfallConnect API for audio and texture loading, extracted from the
[DaggerfallUnity](<https://github.com/Interkarma/daggerfall-unity/>)
project
