autocmd BufEnter * setlocal tabstop=2     " number of spaces for <Tab>
autocmd BufEnter * setlocal softtabstop=2 " tabs at start of line
autocmd BufEnter * setlocal shiftwidth=2  " number of cols for indent operations
" no automatic newlines
autocmd BufEnter * setlocal formatoptions-=t
autocmd BufEnter * setlocal formatoptions-=c
autocmd BufEnter * setlocal formatoptions-=l
