﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DaggerfallConnect;
using DaggerfallConnect.Arena2;
using DaggerfallConnect.Utility;
using Newtonsoft.Json;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace DaggerFile {
  class Program {
    // pixels are transparent when alpha component is equal to alpha index
    const int ALPHA_INDEX = 0;

    static void Main(string[] args) {
      TextWriter stderr = Console.Error;

      // arena2 path string
      String arena2_path;
      if (args.Length > 0) {
        arena2_path = args[0];
      } else {
        String yaml = File.ReadAllText("config.yaml");
        var input = new StringReader(yaml);
        var deserializer = new Deserializer();
        var config = deserializer.Deserialize<Config>(input);
        arena2_path = config.arena2_path;
      }
      if (!Directory.Exists(arena2_path)) {
        void ExitArena2PathNotFound() {
          stderr.WriteLine("Error: ARENA2 path does not exist.");
          Environment.Exit(-1);
        };
        // try changing case
        var parent_dir = Directory.GetParent(arena2_path).FullName;
        if (Path.GetFileName(arena2_path) == "ARENA2") {
          arena2_path = Path.Combine(parent_dir, "arena2");
        } else if (Path.GetFileName(arena2_path) == "arena2") {
          arena2_path = Path.Combine(parent_dir, "ARENA2");
        } else {
          ExitArena2PathNotFound();
        }
        if (!Directory.Exists(arena2_path)) {
          ExitArena2PathNotFound();
        }
      }
      stderr.WriteLine("ARENA2 path: {0}", arena2_path);

      // validate arena2 path
      DFValidator.ValidationResults validation_results;
      DFValidator.ValidateArena2Folder(arena2_path, out validation_results);
      if (!validation_results.AppearsValid) {
        stderr.WriteLine("Error: Failed to validate ARENA2 path: {0}",
          JsonConvert.SerializeObject(validation_results));
        Environment.Exit(-1);
      }

      Stream stdout = Console.OpenStandardOutput();

      //////////////////////////////////////////////////////////////////////////
      //  graphics                                                            //
      //////////////////////////////////////////////////////////////////////////

      // load functions
      Func<string, TextureFile> load_texture = path => new TextureFile(
        Path.Combine(arena2_path, path), FileUsage.UseMemory, true);

      Func<string, ImgFile> load_img = path => {
        var img_file = new ImgFile(Path.Combine(arena2_path, path),
          FileUsage.UseMemory, true);
        img_file.LoadPalette(Path.Combine(arena2_path, img_file.PaletteName));
        return img_file;
      };

      Func<string, CifRciFile> load_cif = path => {
        var cif_file = new CifRciFile(Path.Combine(arena2_path, path),
          FileUsage.UseMemory, true);
        cif_file.LoadPalette(Path.Combine(arena2_path, cif_file.PaletteName));
        return cif_file;
      };

      { // CORT01I0.IMG: bg court
        var img_cort = load_img("CORT01I0.IMG");
        WriteImage(ref stdout, ref img_cort);
      }

      { // DANK02I0.IMG: bg sky
        var img_dank = load_img("DANK02I0.IMG");
        WriteImage(ref stdout, ref img_dank);
      }

      { // CUST08I0.IMG: cursor sword
        var img_cust = load_img("CUST08I0.IMG");
        WriteImage(ref stdout, ref img_cust);
      }

      { // TEXTURE.380: blood+gore
        const int RECORD_BLOOD  = 0;
        const int RECORD_CORPSE = 1;
        var texture = load_texture("TEXTURE.380");
        { // blood
          var frame_count = texture.GetFrameCount(RECORD_BLOOD);
          for (int i = frame_count-1; i >= 0; i -= 1) {
            WriteImage(ref stdout, ref texture, RECORD_BLOOD, i);
          }
        }
        { // corpse
          WriteImage(ref stdout, ref texture, RECORD_CORPSE, 0);
        }
      }

      { // WEAPON04.CIF: weapon sword
        var cif = load_cif("WEAPON04.CIF");
        for (int i = 0; i < cif.RecordCount; i += 1) {
          var frame_count = cif.GetFrameCount(i);
          for (int j = 0; j < frame_count; j += 1) {
            WriteImage(ref stdout, ref cif, i, j);
          }
        }
      }

      { // TEXTURE.489: mage-thief (F)
        var texture = load_texture("TEXTURE.489");
        for (int i = 0; i < texture.RecordCount; i += 1) {
          var frame_count = texture.GetFrameCount(i);
          for (int j = 0; j < frame_count; j += 1) {
            WriteImage(ref stdout, ref texture, i, j);
          }
        }
      }
      { // TEXTURE.482: fighter-light (M)
        var texture = load_texture("TEXTURE.482");
        for (int i = 0; i < texture.RecordCount; i += 1) {
          var frame_count = texture.GetFrameCount(i);
          for (int j = 0; j < frame_count; j += 1) {
            WriteImage(ref stdout, ref texture, i, j);
          }
        }
      }

      //////////////////////////////////////////////////////////////////////////
      //  audio                                                               //
      //////////////////////////////////////////////////////////////////////////

      //
      //  DAGGER.SND: wav sound effects
      //
      SndFile snd_file = new SndFile(
        Path.Combine(arena2_path, SndFile.Filename), FileUsage.UseMemory, true);

      const int SOUND_FIRE = 420;
      const int SOUND_TICK = 360;
      const int SOUND_FOOTSTEP_SOFT1 = 331;
      const int SOUND_FOOTSTEP_SOFT2 = 310;
      const int SOUND_CORPSE_FALL = 15;
      const int SOUND_SWING_MED = 347;
      const int SOUND_HIT_MELEE1 = 108;
      const int SOUND_HIT_MELEE2 = 109;
      const int SOUND_HIT_MELEE3 = 110;

      WriteSound(ref stdout, ref snd_file, SOUND_FIRE);
      WriteSound(ref stdout, ref snd_file, SOUND_TICK);
      WriteSound(ref stdout, ref snd_file, SOUND_FOOTSTEP_SOFT1);
      WriteSound(ref stdout, ref snd_file, SOUND_FOOTSTEP_SOFT2);
      WriteSound(ref stdout, ref snd_file, SOUND_CORPSE_FALL);
      WriteSound(ref stdout, ref snd_file, SOUND_SWING_MED);
      WriteSound(ref stdout, ref snd_file, SOUND_HIT_MELEE1);
      WriteSound(ref stdout, ref snd_file, SOUND_HIT_MELEE2);
      WriteSound(ref stdout, ref snd_file, SOUND_HIT_MELEE3);

      //
      //  MIDI.BSA: song files
      //
      BsaFile songs_file = new BsaFile(
        Path.Combine(arena2_path, "MIDI.BSA"), FileUsage.UseMemory, true);

      // GDUNGN4.HMI: title menu song
      WriteSong(ref stdout, ref songs_file, "GDUNGN4.HMI");
      // 30.HMI: deathmatch song
      WriteSong(ref stdout, ref songs_file, "30.HMI");

      stdout.Close();
    }

    //
    //  utility functions
    //

    static void WriteImage(ref Stream stdout, ref TextureFile texture_file,
      int record=0, int frame=0, int alpha_index=ALPHA_INDEX)
    {
      #if DEBUG
      TextWriter stderr = Console.Error;
      stderr.WriteLine(texture_file.FilePath);
      #endif

      var dfbmp = texture_file.GetDFBitmap(record, frame);
      var offset = texture_file.GetOffset(record);
      //var scale = texture_file.GetScale(record);

      ImageHeader header;
      header.width = dfbmp.Width;
      header.height = dfbmp.Height;
      header.offset_x = offset.X;
      header.offset_y = offset.Y;
      var header_json = JsonConvert.SerializeObject(header);
      #if DEBUG
      stderr.WriteLine(header_json);
      #endif
      Console.Write(header_json);
      var pixels = dfbmp.GetColor32(alpha_index);
      for (int i = 0; i < pixels.Length; i += 1) {
        var color = pixels[i];
        stdout.WriteByte(color.r);
        stdout.WriteByte(color.g);
        stdout.WriteByte(color.b);
        stdout.WriteByte(color.a);
      }
    }

    static void WriteImage(ref Stream stdout, ref ImgFile img_file,
      int alpha_index=ALPHA_INDEX)
    {
      #if DEBUG
      TextWriter stderr = Console.Error;
      stderr.WriteLine(img_file.FilePath);
      #endif

      var dfbmp = img_file.GetDFBitmap();
      var offset = img_file.ImageOffset;
      ImageHeader header;
      header.width = dfbmp.Width;
      header.height = dfbmp.Height;
      header.offset_x = offset.X;
      header.offset_y = offset.Y;
      var header_json = JsonConvert.SerializeObject(header);
      #if DEBUG
      stderr.WriteLine(header_json);
      #endif
      Console.Write(header_json);
      var pixels = dfbmp.GetColor32(alpha_index);
      for (int i = 0; i < pixels.Length; i += 1) {
        var color = pixels[i];
        stdout.WriteByte(color.r);
        stdout.WriteByte(color.g);
        stdout.WriteByte(color.b);
        stdout.WriteByte(color.a);
      }
    }

    static void WriteImage(ref Stream stdout, ref CifRciFile cif_file,
      int record=0, int frame=0, int alpha_index=ALPHA_INDEX)
    {
      #if DEBUG
      TextWriter stderr = Console.Error;
      stderr.WriteLine(cif_file.FilePath);
      #endif

      var dfbmp = cif_file.GetDFBitmap(record, frame);
      var offset = cif_file.GetOffset(record);

      ImageHeader header;
      header.width = dfbmp.Width;
      header.height = dfbmp.Height;
      header.offset_x = offset.X;
      header.offset_y = offset.Y;
      var header_json = JsonConvert.SerializeObject(header);
      #if DEBUG
      stderr.WriteLine(header_json);
      #endif
      Console.Write(header_json);
      var pixels = dfbmp.GetColor32(alpha_index);
      for (int i = 0; i < pixels.Length; i += 1) {
        var color = pixels[i];
        stdout.WriteByte(color.r);
        stdout.WriteByte(color.g);
        stdout.WriteByte(color.b);
        stdout.WriteByte(color.a);
      }
    }

    static void WriteSound(ref Stream stdout, ref SndFile snd_file, int record) {
      #if DEBUG
      TextWriter stderr = Console.Error;
      #endif

      DFSound sound;
      if (!snd_file.GetSound(record, out sound)) {
          TextWriter stderr = Console.Error;
          stderr.WriteLine("Error: failed to load sound {0}", record);
          Environment.Exit(-1);
      }
      SoundHeader header;
      header.wave_bytes_size = sound.WaveHeader.Length + sound.WaveData.Length;
      #if DEBUG
      stderr.WriteLine("DAGGER.SND[{0}] \"{1}\"", record, sound.Name);
      #endif
      var header_json = JsonConvert.SerializeObject(header);
      #if DEBUG
      stderr.WriteLine(header_json);
      #endif
      Console.Write(header_json);
      stdout.Write(sound.WaveHeader);
      stdout.Write(sound.WaveData);
    }

    static void WriteSong(ref Stream stdout, ref BsaFile songs_file,
      string name)
    {
      #if DEBUG
      TextWriter stderr = Console.Error;
      #endif

      var record = songs_file.GetRecordIndex(name);
      var song_bytes = songs_file.GetRecordBytes(record);
      SongHeader header;
      header.hmi_bytes_size = song_bytes.Length;
      #if DEBUG
      stderr.WriteLine("MIDI.BSA[{0}] \"{1}\"", record, name);
      #endif
      var header_json = JsonConvert.SerializeObject(header);
      #if DEBUG
      stderr.WriteLine(header_json);
      #endif
      Console.Write(header_json);
      stdout.Write(song_bytes);
    }

  }

  public class Config {
    public String arena2_path;
  }

  public struct ImageHeader {
    public int width;
    public int height;
    public int offset_x;
    public int offset_y;
  }

  public struct SoundHeader {
    public int wave_bytes_size;
  }

  public struct SongHeader {
    public int hmi_bytes_size;
  }

}
