# DaggerFile

> uses DaggerfallConnect API from the [Daggerfall
> Unity](https://github.com/interkarma/daggerfall-unity) project to export
> textures, sounds, and midi for use by the [Df
> Deathmatch](https://gitlab.com/spearman/dfdm) client


## Building

To run the source code in place:

```
$ dotnet run > /dev/null
```

To build a self-contained release for a target
[runtime](https://docs.microsoft.com/en-us/dotnet/core/rid-catalog) (Linux
x86-64):

```
$ dotnet publish -c Release -r linux-x64
```

(Windows x86-64):

```
$ dotnet publish -c Release -r win-x64
```

will create a self-contained release in
`./bin/Release/netcoreapp2.2/<runtime>/publish/`.

To run the DFDM client, the contents of this directory should be placed in local
directory `/path/to/dfdm/DaggerFile/` together with the `config.yaml` file.
